'use strict';
require('dotenv').config();
const Fetch = require('node-fetch');

const connectBitcoinTradeGame = () => {
  return Fetch(process.env.BITCOIN_TRADE_GAME_API_URL + '/oauth/api', {
    method: 'POST',
    body: JSON.stringify({
      key: process.env.KEY,
      secret: process.env.SECRET
    }),
    credentials: "same-origin"
  })
  .then((response) => {
    console.log(response.headers._headers['set-cookie'][0]);
    return response.headers._headers['set-cookie'][0].split(';')[0];
  });
}

const getAccount = (authCookie) => {
  return Fetch(process.env.BITCOIN_TRADE_GAME_API_URL + '/api/accounts/mine', {
    credentials: "same-origin",
    headers: {
      'Cookie' : authCookie
    }
  }).then((response) => response.json());
}

const execute = () => {
  connectBitcoinTradeGame()
  .then(cookie => Promise.all([cookie, getAccount(cookie)]))
  .then(([cookie, account]) => {
    console.log(account);
  })
  .catch(err => console.log(err))
  ;
}

execute();
