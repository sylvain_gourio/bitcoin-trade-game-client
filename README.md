# bitcoin-trade-game-client

> Client for bitcoin-trade-game

## Build Setup

``` bash
# clone repository
git clone git@bitbucket.org:sylvain_gourio/bitcoin-trade-game-client.git && cd bitcoin-trade-game-client

# use current version of node / npm
nvm use

# install dependencies
npm install
```

## Add a .env file with the following
``` txt
BITCOIN_TRADE_GAME_API_URL=http://api.bitcoin-trade-game.com/ 
SECRET=<your secret>
KEY=<your key>
```
To get yout key and secret : http://www.bitcoin-trade-game.com/#/use-api

## run
``` bash
# run project
node index.js

```
